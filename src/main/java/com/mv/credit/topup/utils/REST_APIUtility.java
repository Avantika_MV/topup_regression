package com.mv.credit.topup.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.simple.parser.ParseException;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;
import java.net.URL;

public class REST_APIUtility {
    private static final Logger _logger = LogManager.getLogger(REST_APIUtility.class.getName());

    public static Response postCall (String url, String payload) throws IOException, ParseException {
        RequestSpecification request = RestAssured.given();
        Response response = null;
        _logger.info("End point : " + url);
        try {
            response = request.header("Content-Type", "application/json")
                    .and()
                    .body(payload)
                    .when()
                    .post(new URL(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
