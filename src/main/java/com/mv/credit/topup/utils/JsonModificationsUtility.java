package com.mv.credit.topup.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class JsonModificationsUtility {

    public static JSONObject modifyPayloadForChain(JSONObject jsonPayload, String chain, String source) throws IOException, ParseException {

        RandomGeneratorUtility rguObj = new RandomGeneratorUtility();
        String la_id = rguObj.generateLaId();
        String lan = rguObj.generateLan();
        JSONObject updatedPayload = JSONUtility.getPayloadAsString(jsonPayload, "loanApplicationInfo", "loanApplicationId", la_id);
        updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanApplicationNumber", lan);


        if (source.contains("Growth")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "isGrowth", "true");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "schemeName", "Growth");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "programName", "Regular");
        }
        if (!source.contains("Growth")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "isGrowth", "false");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "schemeName", "Growth");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "programName", "Covid");
        }
        if (source.contains("Topup")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "isTopup", "true");
        }
        if (!source.contains("Topup")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "isTopup", "false");
        }


        if (chain.contains("_TERM-SE_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employmentType", "Self-Employed");
        }
        if (chain.contains("_TERM-SAL_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employmentType", "Salaried");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employerName", "TOSHIBA TRANSMISSION & DISTRIBUTION SYSTEMS (INDIA) PRIVATE LIMITED");
        }

        if (chain.contains("_COVID") || chain.contains("_GROWTH_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "income", "23000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "declaredIncome", "23000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "opsCalculatedIncome", "23000");
            if (source.contains("Growth")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "55000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "55000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "18");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "2475");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "3562");
            }
            if (source.contains("Topup")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "45000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "45000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "15");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "2025");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "3416");
            }
                if (!source.contains("Growth") && !source.contains("Topup")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "70000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "70000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "15");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "3150");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "5313");
            }
        }
        if (!chain.contains("_COVID") && !chain.contains("_GROWTH_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "income", "43000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "declaredIncome", "43000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "opsCalculatedIncome", "43000");
            if (source.contains("Growth")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "65000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "65000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "18");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "2925");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "4210");
            }
            if (source.contains("Topup")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "55000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "55000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "18");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "2475");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "3562");
            }
            if (!source.contains("Growth") && !source.contains("Topup")) {
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanAmount", "80000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedAmount", "80000");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanTenure", "18");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedRoi", "20");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "rawProcessingFee", "3600");
                updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appliedEmi", "5181");
            }
        }

        if (chain.contains("_FICCL_") && !chain.contains("_FICCL_LOC_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerId", "4");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerName", "FICCL");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "4");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "4");
        }
        if (chain.contains("_FICCL_LOC_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerId", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerName", "FICCL_LOC"); //To-Do
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "6");
        }
        if (chain.contains("_DMI_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerId", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "currentLoanDTO", "partnerName", "DMI");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "3");
        }

        if (chain.contains("_XIAOMI")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appName", "com.mi.moneyview");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "channelName", "XIAOMI");
        }
        if (!chain.contains("_XIAOMI")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appName", "com.whizdm.moneyview.loans.topup");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "channelName", "MVL_APP");
        }


        // System.out.println(jsonObject);
        return updatedPayload;

    }

}
