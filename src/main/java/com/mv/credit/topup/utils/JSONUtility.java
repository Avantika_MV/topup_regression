package com.mv.credit.topup.utils;

import java.io.FileReader;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JSONUtility {

    private static final Logger _logger = LogManager.getLogger(JSONUtility.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    protected static final String CURRENT_DIR = System.getProperty("user.dir");

    public static JSONObject readPayload(String fileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(CURRENT_DIR + "/test_data/"+fileName+".json"));
        JSONObject jsonObject = (JSONObject)obj;
        // System.out.println(jsonObject);
        return jsonObject;
    }

    public static String toJsonString(Object sourceObject) {
        String jsonString = null;
        try {
            jsonString = OBJECT_MAPPER.writeValueAsString(sourceObject);
        } catch (final JsonProcessingException e) {
            _logger.error("Could not serialize entity type {} failed due to ", sourceObject.getClass(), e);
        }
        return jsonString;
    }

    public static final <T> T toObject(final String str, final Class<T> objectType) {
        T readValue = null;
        try {
            readValue = OBJECT_MAPPER.readValue(str, objectType);
        } catch (final IOException e) {
            _logger.error("Could not deserialize entity type {} failed due to ", objectType.getClass(), e);
        }
        return readValue;
    }

    public static JSONObject modifyPayload(JSONObject jsonPayload, String chain, String source) throws IOException, ParseException {
        jsonPayload = JsonModificationsUtility.modifyPayloadForChain(jsonPayload, chain, source);
        // System.out.println(jsonObject);
        return jsonPayload;
    }

    public static JSONObject getPayloadAsString(JSONObject jsonObject, String attributePath, String attributeValue, String newValue) throws IOException, ParseException {
        // get node which needs to be changed
        JSONObject attributePathInfo = (JSONObject) jsonObject.get(attributePath);
        //change the value
        attributePathInfo.put(attributeValue, newValue);
        _logger.info("Payload Changed ::::  " + attributeValue + " = " + newValue);
        return jsonObject;
    }

}
