package com.mv.credit.topup.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class DB_ConnectionsUtility {

    static String DBDomain = "jdbc:mysql://";

    private static final Logger _logger = LogManager.getLogger(DB_ConnectionsUtility.class.getName());


    public static Connection getDBConnection(String DB_host, String DB_username, String DB_password) throws ClassNotFoundException, SQLException {
        Connection conn = null;
        if (!DB_host.equalsIgnoreCase("null")) {
            System.out.println("------------------Starting DB Connection------------------");
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(DBDomain + DB_host, DB_username, DB_password);
        }
        return conn;
    }

    public static String selectQueryResultString(String query, Connection connection) {
        ResultSet rs = null;
        String text = "";
        try {

            Statement stmt = connection.createStatement();
            _logger.info("Firing select query ::: " + query);


            rs = stmt.executeQuery(query);
            //writefile.writeToFile("\n"+query+"\n");


            //System.out.println("current " + tableName + " is:");
            try {
                if (!rs.isBeforeFirst()) {
                    _logger.warn("No Rows Found");
                }
                else {
                    while (rs.next()) {
                        for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                            text+=" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i) + ",";
                            System.out.print(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
                        }
                        text+="\n";
                        System.out.println();
                    }

                }
                _logger.info("Select query has been successfully executed");
            } catch (Exception e) {
                _logger.error("Problem in select query, exception " + e.getMessage());
            }

        } catch (Exception e) {
            _logger.error("Problem in creating statement, exception " + e.getMessage());
        }
        return text;
    }

    public static ResultSet selectQueryResultSet(String query, Connection connection) {
        ResultSet rs = null;
        String text = "";
        try {

            Statement stmt = connection.createStatement();
            _logger.info("Firing select query ::: " + query);

            rs = stmt.executeQuery(query);
            //writefile.writeToFile("\n"+query+"\n");


            //System.out.println("current " + tableName + " is:");
            try {
                if (!rs.isBeforeFirst()) {
                    _logger.warn("No Rows Found");
                }
                else {
                    while (rs.next()) {
                        for (int i = 1; i < rs.getMetaData().getColumnCount() + 1; i++) {
                            text+=" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i) + ",";
                            System.out.print(" " + rs.getMetaData().getColumnName(i) + "=" + rs.getObject(i));
                        }
                        text+="\n";
                        System.out.println();
                        return rs;
                    }

                }
                _logger.info("Select query has been successfully executed");
            } catch (Exception e) {
                _logger.error("Problem in select query, exception " + e.getMessage());
            }

        } catch (Exception e) {
            _logger.error("Problem in creating statement, exception " + e.getMessage());
        }
        return rs;
    }

    public static String updateQuery(String query, Connection connection) {
        String flag = "failure";
        try {
            _logger.info("Firing update query ::: " + query);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
            //writefile.writeToFile("\n"+query+"\n");
            flag = "success";
            _logger.info("Update query has been successfully executed");
        } catch (Exception e) {
            _logger.error("Problem in update query, exception " + e.getMessage());
        }
        return "\"update\":\""+flag+"\"";
    }

}
