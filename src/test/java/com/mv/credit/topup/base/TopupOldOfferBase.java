package com.mv.credit.topup.base;

import io.restassured.path.json.JsonPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TopupOldOfferBase {

    private static final Logger _logger = LogManager.getLogger(TopupOldOfferBase.class.getName());

    public static int get_oldLoanInterestRate(String payloadString) {

        int oldLoanInterestRate = JsonPath.from(payloadString).getInt("currentLoanDTO.roi");
        _logger.info("oldLoanInterestRate ::: " + oldLoanInterestRate);
        return oldLoanInterestRate;

    }

    public static int get_oldLoanEMI(String payloadString) {

        int oldLoanEMI = JsonPath.from(payloadString).getInt("currentLoanDTO.loanEMI");
        _logger.info("oldLoanEMI ::: " + oldLoanEMI);
        return oldLoanEMI;

    }

    public static int get_oldLoanTenure(String payloadString) {

        int oldLoanTenure = JsonPath.from(payloadString).getInt("currentLoanDTO.loanTenure");
        _logger.info("oldLoanTenure ::: " + oldLoanTenure);
        return oldLoanTenure;

    }

    public static int get_oldLoanAmount(String payloadString) {

        int oldLoanAmount = JsonPath.from(payloadString).getInt("currentLoanDTO.loanAmount");
        _logger.info("oldLoanAmount ::: " + oldLoanAmount);
        return oldLoanAmount;

    }

    public static int get_oldLoanPartnerId(String payloadString) {

        int oldLoanPartnerId = JsonPath.from(payloadString).getInt("currentLoanDTO.partnerId");
        _logger.info("oldLoanPartnerId ::: " + oldLoanPartnerId);
        return oldLoanPartnerId;

    }

}
