package com.mv.credit.topup.base;

import com.mv.credit.topup.utils.DB_QueriesUtility;
import io.restassured.path.json.JsonPath;

import org.testng.Assert;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class TopupOfferValidationsBase extends TopupTestBase {

    private static final Logger _logger = LogManager.getLogger(TopupOfferValidationsBase.class.getName());


    public static String validate_chainName(String responseBody) {

        String chainName = JsonPath.from(responseBody).getString("body.chainInfoResponse.chainName");
        _logger.info("chainName ::: " + chainName);
        Assert.assertTrue(chainName.startsWith("TOPUP_"));
        return chainName;

    }

    public static void validate_offerFound(String responseBody, String chainName, String testChain) {

        String offerMessage = JsonPath.from(responseBody).getString("body.offerResponse.message");
        _logger.info("offerMessage ::: " + offerMessage);
        Assert.assertTrue(offerMessage.equalsIgnoreCase("Offers found"));

        Boolean offerAvailable = JsonPath.from(responseBody).getBoolean("body.offerResponse.offerAvailable");
        _logger.info("offerAvailable ::: " + offerAvailable);
        Assert.assertTrue(offerAvailable);

        String message = JsonPath.from(responseBody).getString("message");
        _logger.info("message ::: " + message);
        Assert.assertTrue(message.equalsIgnoreCase("Offer Assigned"));

        Assert.assertTrue(chainName.equalsIgnoreCase(testChain));

    }

    public static void validate_offerAmount(String responseBody, String chainName) throws SQLException {

        double loanAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.loanAmount");
        _logger.info("loanAmount ::: " + loanAmount);
        //double topupMin = 5000.0; // read from directive as per chainName
        double topupMin = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_amount_floor","chain_name",chainName);
        //double topupMax = 500000.0; // read from directive as per chainName
        double topupMax = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_amount_ceil","chain_name",chainName);
        Assert.assertTrue(loanAmount >= topupMin && loanAmount <= topupMax);

    }

    public static void validate_offerProductId(String responseBody, String chainName) throws SQLException {

        String loanProductId = JsonPath.from(responseBody).getString("body.offerResponse.bestOffer.loanProductId");
        _logger.info("loanProductId ::: " + loanProductId);
        // read from directive as per chainName
        String product_id = DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","product_id","chain_name",chainName).toString();
        Assert.assertTrue(loanProductId.equalsIgnoreCase(product_id));

    }

    public static void validate_offerTenure(String responseBody, String chainName) throws SQLException {

        int loanTenure = JsonPath.from(responseBody).getInt("body.offerResponse.bestOffer.loanTenure");
        _logger.info("loanTenure ::: " + loanTenure);
        //int topupMin = 6; // read from directive as per chainName
        int topupMin = (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_tenure_floor","chain_name",chainName);
        //int topupMax = 60; // read from directive as per chainName
        int topupMax = (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_tenure_ceil","chain_name",chainName);
        Assert.assertTrue(loanTenure >= topupMin && loanTenure <= topupMax);

    }

    public static void validate_offerROI(String responseBody, String chainName) throws SQLException {

        double interestRate = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.interestRate");
        _logger.info("interestRate ::: " + interestRate);
        //double topupMin = 16.0; // read from directive as per chainName
        double topupMin = (double) (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","interest_rate_floor","chain_name",chainName);
        //double topupMax = 39.0; // read from directive as per chainName
        double topupMax = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","interest_rate_ceil","chain_name",chainName);
        Assert.assertTrue(interestRate >= topupMin && interestRate <= topupMax);

    }

    public static void validate_offerInterestRateWithRespectToOldInterestRate(String responseBody, String chainName, int oldInterestRate) {

        double newInterestRate = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.interestRate");
        _logger.info("newInterestRate ::: " + newInterestRate);
        double topupMin = -2.0; // TO-DO read from directive as per chainName
        double topupMax = 0.0; // TO-DO read from directive as per chainName
        double InterestRateDifference = newInterestRate - oldInterestRate;
        _logger.info("InterestRateDifference ::: " + InterestRateDifference);
        Assert.assertTrue(InterestRateDifference >= topupMin && InterestRateDifference <= topupMax);

    }

    public static void validate_offerPartnerId(String responseBody, String chainName, int oldPartnerId) {

        String newPartnerId = JsonPath.from(responseBody).getString("body.chainInfoResponse.partner");
        _logger.info("newPartnerId ::: " + newPartnerId);
        Assert.assertTrue(newPartnerId.equalsIgnoreCase(Integer.toString(oldPartnerId)));

    }

    public static void validate_offerMaxEMIWithRespectToOldEMI(String responseBody, String chainName, String source, int oldLoanEMI) {

        double newBestOfferEmi = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.emi");
        _logger.info("newBestOfferEmi ::: " + newBestOfferEmi);
        double emiCap = 1.5; // TO-DO read from directive as per topup type
        if (source.contains("Growth"))
            emiCap = 1.2; // TO-DO read from directive as per topup type
        else if (source.contains("Topup"))
            emiCap = 1.0; // TO-DO read from directive as per topup type
        _logger.info("required emiCapping ::: " + emiCap);
        Assert.assertTrue(newBestOfferEmi <= emiCap * oldLoanEMI);

    }

    public static void validate_pentileUpshift(String responseBody, String chainName) {

        int pentile = JsonPath.from(responseBody).getInt("body.transactionData.pentile");
        _logger.info("pentile ::: " + pentile);
        int bureauSeptile = JsonPath.from(responseBody).getInt("body.transactionData.bureauSeptile");
        _logger.info("bureauSeptile ::: " + bureauSeptile);
        if (bureauSeptile > 1 && bureauSeptile < 7)
            Assert.assertTrue(pentile == bureauSeptile - 1); // As per Business
        else if (bureauSeptile == 1 || bureauSeptile == 7)
            Assert.assertTrue(pentile == bureauSeptile); // As per Business
        else
            _logger.error("BUREAU SEPTILE IS NEGATIVE");

    }

    public static void validate_primaryBureau(String responseBody, String chainName) throws SQLException {

        String creditReportProvider = JsonPath.from(responseBody).getString("body.transactionData.creditReportProvider");
        _logger.info("creditReportProvider ::: " + creditReportProvider);
        String primaryBureau = DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","primary_bureau","chain_name",chainName).toString();
        if (creditReportProvider!=null)
            Assert.assertTrue(creditReportProvider.equals(primaryBureau)); // As per Business
        else
            _logger.error("NO PRIMARY BUREAU CREDIT REPORT PULLED");

    }

    public static void validate_obligationDeduction(String responseBody, String chainName) throws SQLException {

        double obligations = JsonPath.from(responseBody).getDouble("body.transactionData.obligations");
        _logger.info("newObligations ::: " + obligations);
        String creditReportId = JsonPath.from(responseBody).getString("body.transactionData.creditReportId");
        _logger.info("creditReportId ::: " + creditReportId);
        if (!(creditReportId.isEmpty() || creditReportId.equalsIgnoreCase("null"))) {
            double oldObligations = (double) DB_QueriesUtility.readAttributeFromTable(data_conn, "credit_report", "total_obligations", "id", creditReportId);
            if (obligations >= 0.0)
                Assert.assertTrue(obligations <= oldObligations - 500.0); // As per Business
            else
                _logger.error("NEGATIVE NEW OBLIGATIONS");
        }
        else
            _logger.error("NO BUREAU CREDIT REPORT PULLED");

    }

    public static void validate_offerProcFeeRate(String responseBody, String chainName) throws SQLException {

        double loanAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.loanAmount");
        _logger.info("loanAmount ::: " + loanAmount);
        double processingFeeAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.processingFeeAmount");
        _logger.info("processingFeeAmount ::: " + processingFeeAmount);
        double newProcFeeRate = processingFeeAmount/loanAmount * 100.0;
        _logger.info("newProcFeeRate ::: " + newProcFeeRate);
        double proc_charge_floor = 2.0; // As per Business
        //double proc_charge_ceil = 7.0; // read from directive as per chainName
        double proc_charge_ceil = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","proc_charge_ceil","chain_name",chainName);
        Assert.assertTrue(newProcFeeRate >= proc_charge_floor && newProcFeeRate <= proc_charge_ceil);

    }

    public static void validate_offerValid(String responseBody, String chainName, String testChain) {

        String offerMessage = JsonPath.from(responseBody).getString("body.offerResponse.message");
        _logger.info("offerMessage ::: " + offerMessage);
        Assert.assertTrue(offerMessage.equalsIgnoreCase("Current Offer is valid") || offerMessage.equalsIgnoreCase("Revised Offer"));

        Boolean offerAvailable = JsonPath.from(responseBody).getBoolean("body.offerResponse.offerAvailable");
        _logger.info("offerAvailable ::: " + offerAvailable);
        Assert.assertTrue(offerAvailable);

        String message = JsonPath.from(responseBody).getString("message");
        _logger.info("message ::: " + message);
        Assert.assertTrue(message.equalsIgnoreCase("Offer Assigned"));

        Assert.assertTrue(chainName.equalsIgnoreCase(testChain));

    }

}
