package com.mv.credit.topup.test;

import com.mv.credit.topup.ConstantData;
import com.mv.credit.topup.base.TopupOfferValidationsBase;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class TopupTestSourceGrowthTopup extends TopupOfferValidationsBase {

    public static final String source = "GrowthTopup";

    @Test (priority = 1)
    public void offerGeneration_TOPUP_SAL_STANDARD_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_DMI_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 2)
    public void offerGeneration_TOPUP_SAL_GROWTH_DMI_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_COVID;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 3)
    public void offerGeneration_TOPUP_SE_STANDARD_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_STANDARD_DMI_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 4)
    public void offerGeneration_TOPUP_SE_GROWTH_DMI_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_GROWTH_DMI_COVID;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 5)
    public void offerGeneration_TOPUP_SAL_GROWTH_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 6)
    public void offerGeneration_TOPUP_SE_GROWTH_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_GROWTH_DMI_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 7)
    public void offerGeneration_TOPUP_SAL_REGULAR_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_REGULAR_DMI_XIAOMI;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 8)
    public void offerGeneration_TOPUP_SAL_COVID_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_COVID_DMI_XIAOMI;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 9)
    public void offerGeneration_TOPUP_SAL_GROWTH_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_XIAOMI;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 10)
    public void offerGeneration_TOPUP_SAL_STANDARD_FICCL_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_FICCL_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 11)
    public void offerGeneration_TOPUP_SAL_GROWTH_FICCL_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_FICCL_COVID;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 12)
    public void offerGeneration_TOPUP_SAL_STANDARD_FICCL_LOC_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_FICCL_LOC_REGULAR;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 13)
    public void offerGeneration_TOPUP_SAL_GROWTH_FICCL_LOC_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_FICCL_LOC_COVID;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 14)
    public void offerValidation_TOPUP_SAL_STANDARD_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_DMI_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 15)
    public void offerValidation_TOPUP_SAL_GROWTH_DMI_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_COVID;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }


    @Test (priority = 16)
    public void offerValidation_TOPUP_SE_STANDARD_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_STANDARD_DMI_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 17)
    public void offerValidation_TOPUP_SE_GROWTH_DMI_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_GROWTH_DMI_COVID;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 18)
    public void offerValidation_TOPUP_SAL_GROWTH_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 19)
    public void offerValidation_TOPUP_SE_GROWTH_DMI_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SE_GROWTH_DMI_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 20)
    public void offerValidation_TOPUP_SAL_REGULAR_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_REGULAR_DMI_XIAOMI;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 21)
    public void offerValidation_TOPUP_SAL_COVID_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_COVID_DMI_XIAOMI;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 22)
    public void offerValidation_TOPUP_SAL_GROWTH_DMI_XIAOMI() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_DMI_XIAOMI;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 23)
    public void offerValidation_TOPUP_SAL_STANDARD_FICCL_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_FICCL_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 24)
    public void offerValidation_TOPUP_SAL_GROWTH_FICCL_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_FICCL_COVID;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 25)
    public void offerValidation_TOPUP_SAL_STANDARD_FICCL_LOC_REGULAR() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_STANDARD_FICCL_LOC_REGULAR;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

    @Test (priority = 26)
    public void offerValidation_TOPUP_SAL_GROWTH_FICCL_LOC_COVID() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.TOPUP_SAL_GROWTH_FICCL_LOC_COVID;
        ExecuteTest.executeOfferValidationTest(chainStr,source);
    }

}